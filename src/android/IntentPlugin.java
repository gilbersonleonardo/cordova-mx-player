package sevensky.cordova.plugins;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ahmad on 2/3/2017.
 */
public class IntentPlugin extends CordovaPlugin {


    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("startActivity")) {
            String appName = args.getString(0);
            String video = args.getString(1);
            String videoType = args.getString(2);
            this.startActivity(appName,video,videoType, callbackContext);
            return true;
        }
        if (action.equals("installApk")) {
            String apkUrl = args.getString(0);
            this.installApk(apkUrl, callbackContext);
            return true;
        }
        return false;
    }


    private void startActivity(String appName,String video,String videoType, CallbackContext callbackContext) {
        if (appName != null && appName.length() > 0) {
            if (video.length() > 0 && videoType.length() > 0 ) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri videoUri = Uri.parse(video);
                intent.setDataAndType(videoUri, videoType);
                intent.setPackage( appName );
                this.cordova.getActivity().startActivity( intent );
            }else{
                Intent intent = new Intent();
                PackageManager manager = this.cordova.getActivity().getApplicationContext().getPackageManager();
                intent = manager.getLaunchIntentForPackage(appName);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                this.cordova.getActivity().startActivity( intent );
            }

            callbackContext.success(appName);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    private void installApk(String apkUrl, CallbackContext callbackContext) {
        if (apkUrl != null && apkUrl.length() > 0) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(apkUrl), "application/vnd.android.package-archive");
            callbackContext.success(apkUrl);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}
