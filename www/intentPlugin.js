var exec = require('cordova/exec');

function intentPlugin() {

}

intentPlugin.prototype.open = function (packageName,video,callback,fallback) {
    if(arguments.length > 3){
        exec(arguments[2], arguments[3], "IntentPlugin", "startActivity", [arguments[0],arguments[1].url,arguments[1].type]);
    }else{
        exec(arguments[1], arguments[2], "IntentPlugin", "startActivity", [arguments[0],'','']);
    }
}

intentPlugin.prototype.install = function (apkUrl,callback,fallback) {
    exec(arguments[1], arguments[2], "IntentPlugin", "installApk", [arguments[0]]);
}

module.exports = new intentPlugin();